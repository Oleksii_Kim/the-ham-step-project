const images = [
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design1.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design2.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design3.jpg',
        shareLink: '#',
        stopLink: '#'
    },

    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design1.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design2.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design3.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page1.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page2.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page3.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress1.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress2.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress3.jpg',
        shareLink: '#',
        stopLink: '#'
    },
];

const loadImage = [
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design4.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design5.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Graphic Design',
        categoryId: 'graphic-design',
        imageUrl: 'img/graphic-design/graphic-design6.jpg',
        shareLink: '#',
        stopLink: '#'
    },

    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design4.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design5.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Web Design',
        categoryId: 'web-design',
        imageUrl: 'img/web-design/web-design6.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page4.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page5.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Landing Pages',
        categoryId: 'landing-pages',
        imageUrl: 'img/landing-page/landing-page6.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress4.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress5.jpg',
        shareLink: '#',
        stopLink: '#'
    },
    {
        title: 'creative design',
        categoryName: 'Wordpress',
        categoryId: 'wordpress',
        imageUrl: 'img/wordpress/wordpress6.jpg',
        shareLink: '#',
        stopLink: '#'
    },
];

const sectionItems = document.getElementById('gallery-items');
const load = document.getElementById('our-amazing-button');

function createItem(galleryItem) {
    const div = document.createElement('div');
    div.setAttribute('data-tab-id', `${galleryItem.categoryId}`);
    div.setAttribute('class', 'our-amazing-article');

    div.innerHTML = ` <artical>
                    <img class="our-amazing-article-img" src="${galleryItem.imageUrl}" alt="">
                    <div class="our-amazing-article-hover">
                        <div class="our-amazing-article-hover-links">
                            <a class="icon-chain" href="${galleryItem.shareLink}"></a>
                            <a class="icon-stop2" href="${galleryItem.stopLink}"></a>
                        </div>
                        <h2 class="our-amazing-article-hover-title">${galleryItem.title}</h2>
                        <p class="our-amazing-article-hover-text">${galleryItem.categoryName}</p>
                    </div>
                </artical>`;
    return div;
}

function addImages(images) {
    images.forEach(element => {
        sectionItems.appendChild(createItem(element));
    });

}

images.forEach(element => {
    sectionItems.appendChild(createItem(element));
});

$('.our-amazing-button').on('click', function() {
    let c = $(this).data("c") || 0;
    if (c++ < 2) {
        addImages(loadImage);
    }
    if (c >= 2)
        load.style.display = 'none';
    $(this).data("c", c);
});

// load.addEventListener('click',event=>{
//     const activeTab = document.querySelector('.our-amazing-tabs-active');
//     const filteredImages = loadImage.filter(elem=>{
//         if(activeTab.id === "tabs-all"){
//             return true
//         }
//         return  elem.categoryId === activeTab.id
//     })
//     addImages(filteredImages);
// })



