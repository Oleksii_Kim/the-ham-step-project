const navList = document.getElementById('our-service-tabs');
const tabsContent = document.querySelectorAll('[data-id]');
const tabsContentArray = Array.prototype.slice.call(tabsContent);


navList.addEventListener('click', function (event) {
    if (event.target.className === 'our-service-tabs-list') {
        changeOurServiceTab(event.target.id);
        event.preventDefault();
        addClassOnOurServices(event.target);
    }
});


function changeOurServiceTab(id) {
    tabsContentArray.forEach(item => {
        item.style.display = 'none';
        if (item.dataset.id === id) {
            item.style.display = 'flex';
        }
    });
}


function addClassOnOurServices(element) {
    const activeLink = document.querySelector('.our-service-tabs-list-active');
    if (activeLink) {
        activeLink.classList.remove('our-service-tabs-list-active');
    }
    element.classList.add('our-service-tabs-list-active');
}

