$(function () {
    $(".slider-for").slick({
        nextArrow: '<button type="button" class="slick-btn slick-next"></button>',
        prevArrow: '<button type="button" class="slick-btn slick-prev"></button>',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dotsClass: "my-nav-dots",
        asNavFor: ".my-nav-dots",
        appendArrows: $(".dots-container")
    });

    $(".my-nav-dots").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: ".slider-for",
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });
});


