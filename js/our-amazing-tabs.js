const navAmazingList = document.getElementById('our-amazing-tabs');
navAmazingList.addEventListener('click', function (event) {
    const tabsAmazingContent = document.querySelectorAll('[data-tab-id]');
    const tabsAmazingContentArray = Array.prototype.slice.call(tabsAmazingContent);
    if (event.target.className === 'our-amazing-tabs-list') {
        changeAmazingTab(event.target.id, tabsAmazingContentArray);
        event.preventDefault();
        addClassOnAmazingTab(event.target);
    }
    if (event.target.className === 'our-amazing-tabs-list-all'){
        changeOnAmazingTabAll(tabsAmazingContentArray);
        event.preventDefault();
        addClassOnAmazingTab(event.target );
    }
    const container = document.querySelector('.our-amazing .container');
    if(event.target.id === 'tabs-all') {
        container.classList.add('can-load-more');
    } else {
        container.classList.remove('can-load-more')
    }
});
function changeOnAmazingTabAll(tabsAmazingContentArray) {
    tabsAmazingContentArray.forEach(item => {
            item.style.display = 'flex';
    });
}

function changeAmazingTab(id, tabsAmazingContentArray) {
    tabsAmazingContentArray.forEach(item => {
        item.style.display = 'none';
        if (item.dataset.tabId === id) {
            item.style.display = 'flex';
        }
    });
}


function addClassOnAmazingTab(element) {
    const activeLink = document.querySelector('.our-amazing-tabs-active');
    if (activeLink) {
        activeLink.classList.remove('our-amazing-tabs-active');
    }
    element.classList.add('our-amazing-tabs-active');
}

