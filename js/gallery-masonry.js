document.addEventListener("DOMContentLoaded", function(event) {
    let container = document.querySelector('#masonry-container');
    const masonry = new Masonry( container, {
        // Настройки
        columnWidth: 366,
        itemSelector: '.item',
        gutter: 20,
    });
});
